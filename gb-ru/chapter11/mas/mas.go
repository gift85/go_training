/**this need to be in GOPATH visibility? then go install*/
package mas

func Average(xs []float64) float64 {
    total := float64(0)
    for _, x := range xs {
        total += x
    }
    return total / float64(len(xs))
}