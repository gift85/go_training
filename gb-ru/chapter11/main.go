package main

import "fmt"
import "gotr/mas"

func main() {
    xs := []float64{1,2,3,4}
    avg := mas.Average(xs)
    fmt.Println(avg)
}