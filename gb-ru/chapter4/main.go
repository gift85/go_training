package main

import "fmt"

func main() {
    fmt.Print("Enter a number: ")
    var input float64
    fmt.Scanf("%f", &input)
    
    cels := (input - 32) * 5 / 9
    
    fmt.Println(cels)
}